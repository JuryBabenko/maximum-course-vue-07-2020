class Vehicle {
  travelTime() {
    return this.timeTaken;
  }
}

class OnFoot extends Vehicle {
  constructor() {
    super();
    this.timeTaken = 30;
  }
}

class Bus extends Vehicle {
  constructor() {
    super();
    this.timeTaken = 10;
  }
}

class Car extends Vehicle {
  constructor() {
    super();
    this.timeTaken = 5;
  }
}

class TimeSpent {
  travel(transport) {
    return transport.travelTime();
  }
}

const commute = new TimeSpent();

console.log(commute.travel(new OnFoot));
console.log(commute.travel(new Bus));
console.log(commute.travel(new Car));
