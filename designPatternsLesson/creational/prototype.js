const car = {
  wheels: 4,
  init() {
    console.log(`Кол-во колёс: ${this.wheels}, вдаделец: ${this.owner}`);
  }
};

const carWithOwner = Object.create(car, {
  owner: {
    value: 'Юрий',
  },
});

carWithOwner.init();
