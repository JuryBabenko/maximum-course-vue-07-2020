class SimpleMembership {
  constructor(name) {
    this.name = name;
    this.cost = 0;
  }
}

class StandardMembership {
  constructor(name) {
    this.name = name;
    this.cost = 200;
  }
}

class PremiumMembership {
  constructor(name) {
    this.name = name;
    this.cost = 500;
  }
}

class MembershipFactory {
  static list = {
    simple: SimpleMembership,
    standard: StandardMembership,
    premium: PremiumMembership,
  }

  create(name, type = 'simple') {
    const Membership = MembershipFactory.list[type];
    const member = new Membership(name);

    member.type = type;
    member.getInfo = function() {
      console.log(`${this.name} (${this.type}): ${this.cost}`);
    };

    return member;
  }
}

const factory = new MembershipFactory();

const members = [
  factory.create('Yury', 'standard'),
  factory.create('Petya'),
];

members.forEach(m => {
  m.getInfo();
});
