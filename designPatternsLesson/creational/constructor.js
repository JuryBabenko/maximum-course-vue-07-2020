// function Pet(type, age) {
//   this.type = type;
//   this.age = age;
// };

// Pet.prototype.getType = function() {
//   return this.type;
// };
// Pet.prototype.getFullInfo = function() {
//   return `Тип: ${this.type}. Возраст: ${this.age}`
// };

class Pet {
  constructor(type, age) {
    this.type = type;
    this.age = age;
  }

  getFullInfo() {
    return `Тип: ${this.type}. Возраст: ${this.age}`
  }
}

const pushistik = new Pet('cat', 3);

console.log(pushistik.getFullInfo());
